package main

/* BEFORE JSON */
/*
import (
    "io"
    "log"
    "net/http"
    "os"
)

func main() {
    resp, err := http.Get("http://search.twitter.com/search.json?q=%23devfestwph")
    if err != nil {                                         
        log.Fatal(err)
    }
    if resp.StatusCode != http.StatusOK {
        log.Fatal(resp.Status)
    }
    _, err = io.Copy(os.Stdout, resp.Body)
    if err != nil {                       
        log.Fatal(err)
    }
}
*/

/* WITH JSON */
/*
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type Result struct {
    From_User string
    Text string
}

type Response struct {
	Results []Result     
}

func main() {
    resp, err := http.Get("http://search.twitter.com/search.json?q=devfestwph")
    if err != nil {
        log.Fatal(err)
    }
    if resp.StatusCode != http.StatusOK {
        log.Fatal(resp.Status)
    }
    r := new(Response)
    err = json.NewDecoder(resp.Body).Decode(r)
    if err != nil {
        log.Fatal(err)
    }
    for _, child := range r.Results {
        fmt.Println("User: ", child.From_User)
        fmt.Println("Tweet: ", child.Text)
        fmt.Println("------------------------")
    }
}
*/

/* THEN WE MAKE IT PRETTY! */
import (
    "encoding/json"
    "fmt"
    "log"
    "net/http"
    "errors"
)

type Result struct {
    From_User string
    Text string
}

type Response struct {
	Results []Result     
}

func main() {
    items, err := Get("devfestwph")
    if err != nil {
        log.Fatal(err)
    }
    for _, item := range items {
        fmt.Println("User: ", item.From_User)
        fmt.Println("Tweet: ", item.Text)
        fmt.Println("------------------------")
    }
}

func Get(twitter string) ([]Result, error) {
    url := fmt.Sprintf("http://search.twitter.com/search.json?q=%s", twitter)
    resp, err := http.Get(url)                               
    if err != nil {
        return nil, err     }
    defer resp.Body.Close()              
    if resp.StatusCode != http.StatusOK {
        return nil, errors.New(resp.Status)    }
    r := new(Response)                        
    err = json.NewDecoder(resp.Body).Decode(r)
    if err != nil {
        return nil, err    }
    items := make([]Result, len(r.Results))
    for i, child := range r.Results {    
        items[i] = child    }
    return items, nil
}

